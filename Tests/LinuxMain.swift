import XCTest

import SyniadClientTests

var tests = [XCTestCaseEntry]()
tests += SyniadClientTests.allTests()
XCTMain(tests)
