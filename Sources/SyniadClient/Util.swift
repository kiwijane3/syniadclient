//
//  Util.swift
//  SyniadClient
//
//  Created by Jane Fraser on 12/11/20.
//

import Foundation

extension Array where Element == String {
	
	public func filteringDuplicates() -> [String]{
		return self.reduce([]) { (result, element) -> [String] in
			var output = result
			if !output.contains(element) {
				output.append(element);
			}
			return output;
		}
	}
	
}
