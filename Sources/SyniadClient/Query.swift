//
//  Query.swift
//  SyniadClient
//
//  Created by Jane Fraser on 8/02/20.
//

import Foundation

public typealias Query = [String: Any];

public extension Query {
	
	public var asURLComponent: String {
		var output = "";
		let keys = Array(self.keys);
		for index in 0..<keys.count {
			if index == 0 {
				output += "?";
			} else {
				output += "&";
			}
			let key = keys[index];
			output += key;
			output += "=";
			output += representation(of: self[key]!).description;
		}
		debugPrint(output);
		return output;
	}
	
	public func representation(of element: Any) -> String {
		if let string = element as? String {
			return string;
		}
		if let stringArray = element as? [String] {
			print(stringArray.description);
			return stringArray.description;
		}
		if let integer = element as? Int {
			return "\(integer)";
		} else {
			return "null";
		}
	}
	
}
