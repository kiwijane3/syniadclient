//
//  ProjectController.swift
//  AsyncHTTPClient
//
//  Created by Jane Fraser on 12/02/20.
//

import Foundation
import SyniadShared

public class ProjectController: ClientController {
	
	internal var client: SyniadClient;
	
	internal init(client: SyniadClient) {
		self.client = client;
	}
}
