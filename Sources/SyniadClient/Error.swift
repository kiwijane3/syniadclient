//
//  Error.swift
//  AsyncHTTPClient
//
//  Created by Jane Fraser on 6/02/20.
//

import Foundation
#if canImport(Combine)
import Combine
#else
import OpenCL
#endif

public enum NetworkError: Error {
	// Indicates the body could not be processed by the server, either because it could not be decoded to the expected type, or was invalid; I.e, requested the creation of a nameless item.
	case badRequest
	// Body was unexpectedly absent.
	case missingBody
	// Indicates an error response status that was not associated with a specific error, like a 425 Too Early.
	case miscellaneous(statusCode: Int)
}

public enum AccessError: Error {
	// Attempted to make a secured request while client had no credentials set.
	case noLoginInfo
	// Server indicated the request was not authorised.
	case unauthorised
}

public enum ResourceError: Error {
	// Indicates that a file was of the wrong file type.
	case wrongFormat
	case invalid
	// Indeicates the resource could not be found.
	case notFound
	case jsonParseError
}

public enum ServerError: Error {
	case internalError
}

public enum FetchError: Error {
	case invalidated
}

public enum ConnectionError {
	case noConnection
	case interrupted
	case roaming
}

public func responseHasErrorStatus(_ response: HTTPURLResponse) -> Bool {
	let statusCode = response.statusCode
	if statusCode < 300 {
		return false
	} else {
		return true
	}
}

public func error(forStatus statusCode: Int) -> Error {
	switch statusCode {
	case 400:
		return NetworkError.badRequest
	case 401, 403:
		return AccessError.unauthorised
	case 404:
		return ResourceError.notFound
	case 500:
		return ServerError.internalError
	default:
		return NetworkError.miscellaneous(statusCode: statusCode)
	}
}

public enum ImpossibleError: Error {
	case impossible
}


public extension Just {
	
	func eraseWithError() -> AnyPublisher<Output, Error> {
		return mapError { (_) in ImpossibleError.impossible }.eraseToAnyPublisher()
	}
	
}
