//
//  DataController.swift
//  AsyncHTTPClient
//
//  Created by Jane Fraser on 6/02/20.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

public class DataController: ClientController {
	
	internal var client: SyniadClient;
	
	internal init(client: SyniadClient) {
		self.client = client;
	}
	
	public func getData(withID id: ID) -> AnyPublisher<DataContainer, Error> {
		return execute(relativePath: "/data/\(id)", method: "GET").validateStatus().decode(to: DataContainer.self)
	}
 
}
