import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

public class SyniadClient {
	
	internal var urlSession: URLSession
	
	// Information about the user's session.
	internal var loginInfo: LoginInfo?;
	
	public var profile: ID? {
		get {
			return loginInfo?.profile;
		}
	}
	
	internal var token: String? {
		get {
			return loginInfo?.token;
		}
	}

	public var serverUrl: String;
	
	public var accountController: AccountController! = nil;
	
	public var profileController: ProfileController! = nil;
	
	public var dataController: DataController! = nil;
	
	public var itemController: ItemController! = nil;
	
	public var reviewController: ReviewController! = nil;
	
	public var feedController: FeedController! = nil
	
	public var projectController: ProjectController! = nil;
	
	public var localisationController: LocalisationController! = nil;
	
	public var treeController: TreeController! = nil;
	
	public var cancellables = Set<AnyCancellable>()
	
	public init(toServer serverUrl: String, loginInfo: LoginInfo? = nil) {
		urlSession = URLSession()
		self.loginInfo = loginInfo;
		self.serverUrl = serverUrl;
		self.accountController = AccountController(client: self);
		self.profileController = ProfileController(client: self);
		self.dataController = DataController(client: self);
		self.itemController = ItemController(client: self);
		self.reviewController = ReviewController(client: self);
		self.feedController = FeedController(client: self)
		self.projectController = ProjectController(client: self);
		self.localisationController = LocalisationController(client: self);
		self.treeController = TreeController(client: self);
	}
	
}
