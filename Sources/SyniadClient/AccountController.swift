//
//  File.swift
//  
//
//  Created by Jane Fraser on 6/02/20.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

public class AccountController: ClientController {
	
	public var client: SyniadClient;
	
	public init(client: SyniadClient) {
		self.client = client;
	}
	
	public func createAccount(withEmail email: String, username: String, password: String) -> AnyPublisher<LoginInfo, Error> {
		return execute(relativePath: "/createAccount", method: "Post", body: AccountCreationRequest(email: email, username: username, password: password)).validateStatus().decode(to: LoginInfo.self)
			.map { (loginInfo) -> (LoginInfo) in
				self.client.loginInfo = loginInfo;
				return loginInfo;
			}.eraseToAnyPublisher()
	}
	
	public func login(withEmail email: String, password: String) -> AnyPublisher<LoginInfo, Error> {
		return execute(relativePath: "/login", method: "POST", body: LoginRequest(email: email, password: password)).validateStatus().decode(to: LoginInfo.self)
			.map { (loginInfo) -> (LoginInfo) in
				self.client.loginInfo = loginInfo;
				return loginInfo;
			}.eraseToAnyPublisher()
	}
	
	// Validates the given login information, and sets the client to use it if valid. Returns whether the login has worked
	public func use(loginInfo: LoginInfo?) -> AnyPublisher<Bool, Error> {
		guard let loginInfo = loginInfo else {
			return Just(false).eraseWithError()
		}
		self.client.loginInfo = loginInfo;
		// Attempt to validate the loginInfo and return the result.
		return execute(relativePath: "/validate", method: "POST").map { (output) -> Bool in
			guard let response = output.response as? HTTPURLResponse else {
				return false
			}
			if response.statusCode == 200 {
				return true
			} else {
				return false
			}
		}.eraseToAnyPublisher().eraseError()
	}
	
}
