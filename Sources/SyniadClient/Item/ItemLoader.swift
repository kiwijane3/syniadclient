//
//  ItemLoader.swift
//  SyniadIOS
//
//  Created by Jane Fraser on 2/06/20.
//  Copyright © 2020 Luoja Labs. All rights reserved.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

public class ItemLoader {
	
	// Filter used to narrow the search results.
	public var filter: SearchFilter {
		didSet {
			// Cancel any ongoing promises.
			if filter.isSubset(of: oldValue) {
				filterContents();
			} else {
				reset();
			}
		}
	}
	
	public let itemController: ItemController;
	
	public var endIndex: Int {
		get {
			return content.count;
		}
	}
	
	/// The collection containing the elements.
	public private(set) var content: [Item]
	
	public private(set) var allLoaded: Bool = false
	
	public var errorEvent = PassthroughSubject<Error, Never>()
	
	public var fetchCancellable: AnyCancellable?
	
	public var delegate: ItemLoaderDelegate?
	
	public init(controller: ItemController, with filter: SearchFilter, likedBy: ID? = nil) {
		itemController = controller
		self.filter = filter
		content = []
	}
	
	public func page(forIndex index: Int) -> Int {
		return index / pageSize;
	}
	
	// Fetches the next page of data. Returns a future for the updated content.
	public func fetchNext() {
		if allLoaded || fetchCancellable == nil {
			return
		}
		fetchCancellable = itemController.itemSearch(withFilter: filter, skip: content.count).redirectError(to: errorEvent).sink { [weak self] (items) in
			guard self != nil else {
				return
			}
			self!.content.append(contentsOf: items)
			self!.delegate?.itemLoader(updatedContentTo: self!.content)
			self!.fetchCancellable = nil
		}
	}
	
	public func reset() {
		content = [];
		fetchCancellable?.cancel()
		fetchCancellable = nil
		allLoaded = false;
		delegate?.itemLoader(updatedContentTo: content)
		fetchNext()
	}
	
	// Removes elements from the loaded contents that no longer fit the filter.
	public func filterContents() {
		content = content.filter({ (item) -> Bool in
			filter.includes(item: item);
		})
		fetchCancellable?.cancel()
		fetchCancellable = nil
		delegate?.itemLoader(updatedContentTo: content)
		fetchNext()
	}
	
	public func itemUpdated(item: Item) {
		let index = content.firstIndex { (element) -> Bool in
			element.id == item.id
		}
		if let index = index {
			content.remove(at: index)
			content.insert(item, at: index)
		}
	}
	
}

public protocol ItemLoaderDelegate {
	
	func itemLoader(updatedContentTo content: [Item])
	
}
