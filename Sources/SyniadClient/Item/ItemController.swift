//
//  ItemController.swift
//  SyniadClient
//
//  Created by Jane Fraser on 8/02/20.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

// The server defines a limit of 10 by default, so we use this as a page size.
public let pageSize = 10;

public class ItemController: ClientController {
	
	internal unowned var client: SyniadClient;
	
	internal init(client: SyniadClient) {
		self.client = client;
	}

	public func itemSearch(withFilter filter: SearchFilter, skip: Int) -> AnyPublisher<[Item], Error> {
		return execute(relativePath: "/items", method: "GET", body: filter, query: skipQuery(skip)).validateStatus().decode(to: [Item].self)
	}
	
	public func items(savedBy profileId: ID, withFilter filter: SearchFilter, skip: Int) -> AnyPublisher<[Item], Error> {
		return execute(relativePath: "/profile/\(profileId)/savedItems", method: "GET", body: filter, query: skipQuery(skip)).validateStatus().decode(to: [Item].self);
	}
	
	public func count(withFilter filter : SearchFilter) -> AnyPublisher<Int, Error> {
		return execute(relativePath: "/items/count", method: "GET", body: filter).validateStatus().decode(to: Int.self)
	}
	
	public func item(forID itemID: ID) -> AnyPublisher<Item, Error> {
		return execute(relativePath: itemPath(for: itemID)).validateStatus().decode(to: Item.self)
	}
	
	func itemPath(for id: ID) -> String {
		return "/items/\(id)"
	}
	
	public func createItem(using creationRequest: ItemCreationRequest) -> AnyPublisher<Item, Error> {
		return execute(relativePath: "/items", method: "POST", body: creationRequest, requiresLogin: true).validateStatus().decode(to: Item.self)
	}

	public func updateItem(withID target: ID, using updateRequest: ItemUpdateRequest) -> AnyPublisher<Item, Error> {
		return execute(relativePath: "/items/\(target)", method: "POST", body: updateRequest, requiresLogin: true).validateStatus().decode(to: Item.self);
	}
	
	public func postItemContent(toItem target: ID, data: Data, type: DataType) -> AnyPublisher<Item, Error> {
		return execute(relativePath: "/items/\(target)/content", method: "POST", body: DataRequest(for: data, as: type), requiresLogin: true).validateStatus().decode(to: Item.self)
	}
	
	public func postItemImages(_ images: [DataRequest], toItem target: ID) -> AnyPublisher<ItemImageUploadResult, Error> {
		let images = images.filter { (request) -> Bool in
			return request.dataType.isImage
		}
		guard !images.isEmpty else {
			return Fail(error: ResourceError.wrongFormat).eraseToAnyPublisher()
		}
		return execute(relativePath: "/items/\(target)/images", method: "POST", body: images, requiresLogin: true).validateStatus().decode(to: ItemImageUploadResult.self)
	}
	
	public func deleteItemImages(_ imageIDs: [ID], toItem target: ID) -> AnyPublisher<Item, Error> {
		return execute(relativePath: "/items/\(target)/images", method: "DELETE", body: imageIDs, requiresLogin: true).validateStatus().decode(to: Item.self)
	}
	
	public func reorderItemImage(using ordering: [ID: Int], toItem target: ID) -> AnyPublisher<Item, Error> {
		return execute(relativePath: "/items/\(target)/images/ordering", method: "PATCH", body: ordering, requiresLogin: true).validateStatus().decode(to: Item.self)
	}
	
	public func loader(with filter: SearchFilter) -> ItemLoader {
		return ItemLoader(controller: self, with: filter);
	}
	
	public func commonTags(filter: SearchFilter) -> AnyPublisher<[String], Error> {
		// This map is necessary because there are sometimes duplicates in the tag database. Something more functional should be done about this.
		return execute(relativePath: "/tags/common", method: "GET", body: filter).validateStatus().decode(to: [String].self).map { (tags) -> [Tag] in
			return tags.filteringDuplicates();
		}.eraseToAnyPublisher()
	}
	
	public func tags(prefixedBy prefix: String) -> AnyPublisher<[String], Error> {
		return execute(relativePath: "/tags/\(prefix)", method: "GET").validateStatus().decode(to: [String].self).map { (tags) -> [Tag] in
			return tags.filteringDuplicates();
		}.eraseToAnyPublisher()
	}
	
}

