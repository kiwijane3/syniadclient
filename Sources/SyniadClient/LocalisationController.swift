//
//  LocalisationationController.swift
//  AsyncHTTPClient
//
//  Created by Jane Fraser on 3/11/20.
//

import Foundation

public class LocalisationController: ClientController {
	
	public var client: SyniadClient;
	
	public var localisation: [String: String]?;
	
	public init(client: SyniadClient) {
		self.client = client;
	}
	
	/// Fetches the localisation for the given locale from the server.
	public func load(locale: String) {
		execute(relativePath: "/localisation/\(locale)", method: "GET").validateStatus().decode(to: [String: String].self).sink { (completion) in
			switch completion {
			case .failure(let error):
				debugPrint("Encountered network error: \(error)");
			default:
				break
			}
		} receiveValue: { (localisation) in
			self.localisation = localisation
		}.store(in: &client.cancellables)
	}

	
	/// Gives the localised string for the given identifier, or an error display in the form "!!<identifier>!!".
	public func localised(for identifier: String) -> String {
		return localisation?[identifier] ?? "!!\(identifier)!!"
	}
	
}
