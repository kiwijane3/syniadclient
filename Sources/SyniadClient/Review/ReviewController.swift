//
//  ReviewController.swift
//  AsyncHTTPClient
//
//  Created by Jane Fraser on 11/02/20.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

public class ReviewController: ClientController {
	
	internal var client: SyniadClient;
	
	internal init(client: SyniadClient) {
		self.client = client;
	}
	
	public func getReview(forID id: ID) -> AnyPublisher<ItemReview, Error> {
		return execute(relativePath: "/reviews/\(id)").validateStatus().decode(to: ItemReview.self)
	}
	
	public func getReviews(forItem itemID: ID, skip: Int) -> AnyPublisher<[ItemReview], Error> {
		return execute(relativePath: "/items/\(itemID)/reviews", method: "GET", query: skipQuery(skip)).validateStatus().decode(to: [ItemReview].self)
	}
	
	public func getReviewFromCurrentUser(forItem itemID: ID) -> AnyPublisher<ItemReview, Error> {
		return execute(relativePath: currentUserReviewPath(forItem: itemID)).validateStatus().decode(to: ItemReview.self)
	}
	
	func currentUserReviewPath(forItem itemId: ID) -> String {
		return "/items/\(itemId)/reviews/user/\(profile)"
	}
	
	public func postReview(ofItem itemID: ID, with reviewRequest: ItemReviewRequest) -> AnyPublisher<ItemReview, Error> {
		return execute(relativePath: "/items/\(itemID)/reviews", method: "POST", body: reviewRequest, requiresLogin: true).validateStatus().decode(to: ItemReview.self)
	}
	
	public func patchReview(_ reviewID: ID, using patchRequest: ItemReviewUpdateRequest) -> AnyPublisher<ItemReview, Error> {
		return execute(relativePath: "/reviews/\(reviewID)", method: "PATCH", body: patchRequest, requiresLogin: true).validateStatus().decode(to: ItemReview.self)
	}
	
	public func postImages(toReview reviewID: ID, images: [DataRequest]) -> AnyPublisher<ReviewImageUploadResult, Error> {
		let images = images.filter { (dataRequest) -> Bool in
			return dataRequest.dataType.isImage;
		}
		if images.isEmpty {
			return Fail(error: ResourceError.wrongFormat).eraseToAnyPublisher()
		}
		return execute(relativePath: "/reviews/\(reviewID)/images", method: "POST", body: images, requiresLogin: true).validateStatus().decode(to: ReviewImageUploadResult.self)
	}
	
	public func removeImages(fromReview reviewID: ID, withIDs imageIDs: [ID]) -> AnyPublisher<ItemReview, Error> {
		return execute(relativePath: "/reviews/\(reviewID)/images", method: "DELETE", body: imageIDs, requiresLogin: true).validateStatus().decode(to: ItemReview.self);
	}
	
	public func reorderImages(onReview reviewID: ID, using ordering: [ID: Int]) -> AnyPublisher<ItemReview, Error> {
		return execute(relativePath: "/reviews/\(reviewID)/images/ordering", method: "PATCH", body: ordering).validateStatus().decode(to: ItemReview.self)
	}
	
}
