//
//  ReviewLoader.swift
//  SyniadClient
//
//  Created by Jane Fraser on 24/02/21.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

public class ReviewLoader {
	
	public var item: Item? {
		didSet {
			reset()
		}
	}
	
	public let reviewController: ReviewController
	
	public private(set) var content: [ItemReview]
	
	public private(set) var allLoaded: Bool = false
	
	public var errorEvent = PassthroughSubject<Error, Never>()
	
	public var endIndex: Int {
		get {
			return content.count
		}
	}
	
	public var delegate: ReviewLoaderDelegate?
	
	public var fetchCancellable: AnyCancellable?
	
	public init(controller: ReviewController) {
		reviewController = controller
		content = []
	}
	
	public func fetchNext() {
		guard let id = item?.id, !allLoaded, fetchCancellable == nil else {
			return
		}
		fetchCancellable = reviewController.getReviews(forItem: id, skip: content.count).redirectError(to: errorEvent).sink { [weak self] (retrieved) in
			guard self != nil else {
				return
			}
			self!.content.append(contentsOf: retrieved)
			self!.delegate?.reviewLoader(updatedContentsTo: self!.content)
			self!.fetchCancellable = nil
		}

	}
	
	public func reset() {
		content = []
		allLoaded = false
		fetchCancellable?.cancel()
		fetchCancellable = nil
		delegate?.reviewLoader(updatedContentsTo: content)
		fetchNext()
	}
}

public protocol ReviewLoaderDelegate {
	
	func reviewLoader(updatedContentsTo contents: [ItemReview])
	
}

public extension ReviewController {
	
	public func loader() -> ReviewLoader {
		return ReviewLoader(controller: self)
	}
	
}
