//
//  ProfileController.swift
//  AsyncHTTPClient
//
//  Created by Jane Fraser on 6/02/20.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

// For retrieving and updating profiles.
public class ProfileController: ClientController {
	
	internal var client: SyniadClient;

	public init(client: SyniadClient) {
		self.client = client
	}
	
	// Gets the user profile for the currently logged in user.
	public func getSessionProfile() -> AnyPublisher<UserProfile, Error> {
		return execute(relativePath: sessionProfilePath(), method: "GET").validateStatus().decode(to: UserProfile.self)
	}
	
	func sessionProfilePath() -> String {
		return "/profile/\(profile)"
	}
	
	public func getProfile(forID id: ID) -> AnyPublisher<UserProfile, Error> {
		execute(relativePath: "/profile/\(id)", method: "GET").validateStatus().decode(to: UserProfile.self)
	}
	
	public func getItemGroups(forProfileId profile: ID) -> AnyPublisher<[ProfileItemGroupIdentifier], Error> {
		// The user may update the item groups in a way that cannot be detected, never use the cache.
		execute(relativePath: "/profile/\(profile)/itemGroups", method: "GET").validateStatus().decode(to: [ProfileItemGroupIdentifier].self)
	}
	
	public func profile(withId profile: ID, hasSaved itemId: ID) -> AnyPublisher<Bool, Error> {
		execute(relativePath: "/profile/\(profile)/hasSaved/\(itemId)", method: "GET").validateStatus().decode(to: Bool.self)
	}
	
	// Patches the currently logged in profile;
	public func patchProfile(request: UserProfileUpdateRequest) -> AnyPublisher<UserProfile, Error>{
		return execute(relativePath: "/profile/\(profile)", method: "PATCH", body: request, requiresLogin: true).validateStatus().decode(to: UserProfile.self)
	}
	
	public func uploadProfileAvatar(imageData: Data, format: DataType) -> AnyPublisher<UserProfile, Error> {
		guard format.isImage else {
			return Fail(error: ResourceError.wrongFormat).eraseToAnyPublisher()
		}
		return execute(relativePath: "/profile/\(profile)/avatar", method: "POST", body: DataRequest(for: imageData, as: format), requiresLogin: true).validateStatus().decode(to: UserProfile.self)
	}
	
	public func save(item itemID: ID) -> AnyPublisher<Void, Error> {
		return execute(relativePath: "/profile/\(profile)/saved", method: "PATCH", body: SaveRequest(saving: itemID), requiresLogin: true).validateStatus().void()
	}
	
	public func unsave(item itemID: ID) -> AnyPublisher<Void, Error> {
		return execute(relativePath: "/profile/\(profile)/saved", method: "PATCH", body: SaveRequest(removing: itemID), requiresLogin: true)
			.validateStatus().void()
	}
	
	public func subscribe(to profileID: ID) -> AnyPublisher<Void, Error> {
		return execute(relativePath: "/profile/\(profile)/subscriptions", method: "PATCH", body: SubscriptionRequest(subscribingTo: profileID), requiresLogin: true).validateStatus().void();
	}
	
	public func unsubscribe(from profileID: ID) -> AnyPublisher<Void, Error> {
		return execute(relativePath: "/profile/\(profile)/subscriptions", method: "PATCH", body: SubscriptionRequest(unsubscribingTo: profileID), requiresLogin: true).validateStatus().void();
	}
	
}
