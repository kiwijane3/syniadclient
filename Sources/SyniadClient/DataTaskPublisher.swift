//
//  DataTaskPublisher.swift
//  SyniadClient
//
//  Created by Jane Fraser on 19/03/21.
//

import Foundation
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

public extension AnyPublisher where Output == (data: Data, response: URLResponse), Failure == Error {
	
	func decode<T: Decodable>(to: T.Type) -> AnyPublisher<T, Error> {
		return map { (output) -> T? in
			let (data, response) = output
			return try? JSONDecoder().decode(T.self, from: data)
		}.tryMap { (output) -> T in
			if let output = output {
				return output
			} else {
				throw ResourceError.jsonParseError
			}
		}.eraseToAnyPublisher()
	}
	
	func void() -> AnyPublisher<Void, Error> {
		return map { (_) -> Void in
			return Void()
		}.eraseToAnyPublisher()
	}
	
	func text() -> AnyPublisher<String, Error> {
		return map { (output) -> String in
			return String(data: output.data, encoding: .utf8) ?? ""
		}.eraseToAnyPublisher()
	}
	
	func cache(toRelativeURL relativeURL: String) {
		
	}
	
}

public extension AnyPublisher where Output == (data: Data, response: URLResponse), Failure == URLError {
	
	func validateStatus() -> AnyPublisher<(data: Data, response: URLResponse), Error> {
		return tryMap { (output) -> (data: Data, response: URLResponse) in
			if let response = output.response as? HTTPURLResponse, responseHasErrorStatus(response) {
				throw error(forStatus: response.statusCode)
			}
			return output
		}.eraseToAnyPublisher()
	}
	
}

public extension AnyPublisher where Failure == URLError {
	
	func eraseError() -> AnyPublisher<Output, Error> {
		return self.mapError { (error) -> Error in
			return error
		}.eraseToAnyPublisher()
	}
	
}

public extension AnyPublisher {
	
	func redirectError(to subject: PassthroughSubject<Failure, Never>) -> AnyPublisher<Output, Never> {
		`catch` { (error) -> Empty<Output, Never> in
			subject.send(error)
			return Empty(completeImmediately: true)
		}.eraseToAnyPublisher()
	}
	
}

