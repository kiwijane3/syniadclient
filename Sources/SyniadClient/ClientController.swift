//
//  File.swift
//  
//
//  Created by Jane Fraser on 6/02/20.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
import OpenCombineFoundation
#endif

internal protocol ClientController {
	
	var client: SyniadClient {
		get
	}
	
}

internal extension ClientController {
	
	var urlSession: URLSession{
		get {
			return self.client.urlSession;
		}
	}
	
	var profile: ID {
		get {
			return self.client.profile ?? "";
		}
	}
	
	var token: String? {
		get {
			return self.client.token
		}
	}
	
	var serverUrl: String {
		get {
			return self.client.serverUrl;
		}
	}
	
	// Throws an error if the user is not logged into an account.
	func ensureLogin() throws {
		if self.client.loginInfo == nil {
			throw AccessError.noLoginInfo;
		}
	}
	
	
	func execute(relativePath: String, method: String? = nil, query: [URLQueryItem] = [], requiresLogin: Bool = false) -> AnyPublisher<(data: Data, response: URLResponse), URLError> {
		// It we have a current login info, add the authorisation header to the headers.
		if requiresLogin && token == nil {
			return Fail(outputType: (data: Data, response: URLResponse).self, failure: URLError(.userAuthenticationRequired)).eraseToAnyPublisher()
		}
		var components = URLComponents()
		components.scheme = "http";
		components.host = serverUrl;
		components.path = relativePath;
		if !query.isEmpty {
			components.queryItems = query;
		}
		let url = components.url!;
		var request = URLRequest(url: url)
		request.httpMethod = method
		if let token = token {
			request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
		}
		// Indicate the user making the request so that the server can give an appropriate Cache-Control
		request.addValue(profile, forHTTPHeaderField: "User")
		request.cachePolicy = .useProtocolCachePolicy
		return urlSession.dataTaskPublisher(for: request).eraseToAnyPublisher()
	}
	
	func execute<T>(relativePath: String, method: String? = nil, body: T? = nil, query: [URLQueryItem] = [], requiresLogin: Bool = false) -> AnyPublisher<(data: Data, response: URLResponse), URLError> where T: Encodable {
		if requiresLogin && token == nil {
			return Fail(outputType: (data: Data, response: URLResponse).self, failure: URLError(.userAuthenticationRequired)).eraseToAnyPublisher()
		}
		var components = URLComponents();
		components.scheme = "http";
		components.host = serverUrl;
		components.path = relativePath;
		if !query.isEmpty {
			components.queryItems = query;
		}
		var request = URLRequest(url: components.url!)
		if let body = body {
			let bodyData = try! JSONEncoder().encode(body)
			request.httpBody = bodyData
			request.addValue("application/json", forHTTPHeaderField: "Content-Type")
		}
		if let token = token {
			request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
		}
		request.addValue(profile, forHTTPHeaderField: "User")
		request.httpMethod = method
		return urlSession.dataTaskPublisher(for: request).eraseToAnyPublisher()
	}
	
}
