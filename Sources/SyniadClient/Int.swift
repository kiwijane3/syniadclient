//
//  Int.swift
//  AsyncHTTPClient
//
//  Created by Jane Fraser on 17/06/20.
//

import Foundation

public extension Int {
	
	public func ceiling(_ maximum: Int) -> Int{
		if self > maximum {
			return maximum
		} else {
			return self;
		}
	}
	
	public func floor(_ minimum: Int) -> Int {
		if self < minimum {
			return minimum;
		} else {
			return self;
		}
	}
	
	public func bounded(from minimum: Int, to maximum: Int) -> Int {
		self.floor(minimum).ceiling(maximum);
	}
	
}

public func skipQuery(_ skip: Int) -> [URLQueryItem] {
	return [URLQueryItem(name: "skip", value: "\(skip)")];
}

public func timeQuery(priorTo date: Date?) -> [URLQueryItem] {
	guard let date = date else {
		return []
	}
	let formatter = ISO8601DateFormatter()
	let dateString = formatter.string(from: date)
	return [URLQueryItem(name: "priorTo", value: dateString)]
}
