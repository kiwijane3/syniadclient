//
//  TreeController.swift
//  SyniadClient
//
//  Created by Jane Fraser on 3/11/20.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

public class TreeController: ClientController {
	
	public var client: SyniadClient;
	
	public var categoryTree: Tree<CategoryFragment>?;
	
	public var styleTree: Tree<StyleFragment>?;
	
	public var cancellables = Set<AnyCancellable>()
	
	public init(client: SyniadClient) {
		self.client = client;
	}
	
	public func load() {
		execute(relativePath: "/categories", method: "GET").validateStatus().decode(to: Tree<CategoryFragment>.self).sink { (completion) in
			switch completion {
			case .failure(let error):
				debugPrint("Encountered Network Error: \(error)")
			default:
				break
			}
		} receiveValue: { [weak self] (categories) in
			self?.categoryTree = categories
		}.store(in: &cancellables)
		execute(relativePath: "/styles", method: "GET").validateStatus().decode(to: Tree<StyleFragment>.self).sink(receiveCompletion: { (completion) in
			switch completion {
			case .failure(let error):
				debugPrint(error)
			default:
				return
			}
		}, receiveValue: { [weak self] (styles) in
			self?.styleTree = styles
		}).store(in: &cancellables)
	}
	
	public func subCategories(of category: SyniadShared.Category) -> [CategoryFragment] {
		return categoryTree?.children(at: category) ?? [];
	}
	
	public func subStyles(of style: Style) -> [StyleFragment] {
		return styleTree?.children(at: style) ?? [];
	}
	
}
