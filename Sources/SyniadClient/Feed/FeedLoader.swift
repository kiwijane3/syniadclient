//
//  FeedLoader.swift
//  AsyncHTTPClient
//
//  Created by Jane Fraser on 5/03/21.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

public class FeedLoader {
	
	public var feedController: FeedController
	
	// The current items of the feed.
	public var posts: [Post] = []
	
	// The previously loaded items, indexed by first element so they can be reloaded to loaded posts once the first item is encountered.
	public var oldPosts: [Post: [Post]] = [:]
	
	public weak var delegate: FeedLoaderDelegate?
	
	public var errorEvent = PassthroughSubject<Error, Never>()
	
	public var fetchCancellable: AnyCancellable?
	
	public init(feedController: FeedController) {
		self.feedController = feedController
	}
	
	public func fetch() {
		guard fetchCancellable == nil else {
			return
		}
		fetchCancellable = feedController.getPostsForSessionUser(before: posts.last?.date ?? Date()).redirectError(to: errorEvent).sink { [weak self] (posts) in
			guard self != nil else {
				return
			}
			self!.add(posts: posts)
			self?.fetchCancellable = nil
		}
	}
	
	public func refresh() {
		// Store current posts to be reloaded when we reach their point in the stream.
		if !posts.isEmpty {
			oldPosts[posts.first!] = posts
			posts = []
		}
		fetchCancellable?.cancel()
		fetchCancellable = nil
		fetch()
	}
	
	public func add(posts: [Post]) {
		for post in posts {
			let shouldBreak = add(post: post)
			if shouldBreak {
				break
			}
		}
		delegate?.feedLoader(updatedPostsTo: posts)
	}
	
	// Adds the given item to the current posts, and adds any previously loaded posts from oldPosts if the first post is encountered. Returns whether adding should terminate due to adding previously loaded posts.
	public func add(post: Post) -> Bool {
		if let previousPosts = oldPosts[post] {
			oldPosts[post] = nil
			posts.append(contentsOf: previousPosts)
			return true
		} else {
			posts.append(post)
			return false
		}
	}
	
}

public protocol FeedLoaderDelegate: class {
	
	func feedLoader(updatedPostsTo posts: [Post])
	
}

public extension FeedController {
	
	func loader() -> FeedLoader {
		return FeedLoader(feedController: self)
	}
	
}
