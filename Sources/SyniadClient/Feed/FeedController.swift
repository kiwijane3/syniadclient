//
//  FeedController.swift
//  AsyncHTTPClient
//
//  Created by Jane Fraser on 13/02/20.
//

import Foundation
import SyniadShared
#if canImport(Combine)
import Combine
#else
import OpenCombine
#endif

public class FeedController: ClientController {
	
	internal var client: SyniadClient;

	internal init(client: SyniadClient) {
		self.client = client;
	}
	
	public func getPostsFrom(user userID: ID, before latestDate: Date) -> AnyPublisher<[Post], Error> {
		return execute(relativePath: "/profile/\(userID)/posts", method: "GET", query: timeQuery(priorTo: latestDate), requiresLogin: true).validateStatus().decode(to: [Post].self);
	}
	
	public func getPostsForSessionUser(before latestDate: Date) -> AnyPublisher<[Post], Error> {
		return execute(relativePath: "/profile/\(profile)/feed", method: "GET", query: timeQuery(priorTo: latestDate), requiresLogin: true).validateStatus().decode(to: [Post].self)
	}
	
}
