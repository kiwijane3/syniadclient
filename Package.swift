// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SyniadClient",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "SyniadClient",
            targets: ["SyniadClient"]),
    ],
    dependencies: [
		.package(url: "../SyniadShared", from: "1.1.11"),
		.package(url: "https://github.com/OpenCombine/OpenCombine.git", from: "0.12.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "SyniadClient",
			dependencies: [
				"SyniadShared",
				"OpenCombine",
				.product(name: "OpenCombineFoundation", package: "OpenCombine"),
				.product(name:"OpenCombineDispatch", package: "OpenCombine")
			]
		),
        .testTarget(
            name: "SyniadClientTests",
            dependencies: ["SyniadClient"]
		),
    ]
)
